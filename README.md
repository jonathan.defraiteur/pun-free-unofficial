PUN Free unofficial package
===========================

Official links
--------------
- [Asset Store](https://assetstore.unity.com/packages/tools/network/pun-2-free-119922)
- [PUN Documentation](https://doc.photonengine.com/en-us/pun/v2/)
- [Support](http://forum.photonengine.com/)

Setup
-----
```
# Packages/manifest.json

{
  "dependencies": {
    "com.jonathan-defraiteur.pun-free-unofficial": "https://gitlab.com/jonathan.defraiteur/pun-free-unofficial.git",
    ...
  },
}
```
### ⚠ PUN first editor checking may fail

You may get an error "Could not load PhotonServerSettings to update RPCs".  
To avoid this issue, you can prepare your project by adding manually this PhotonServerSetting.asset in a Resources folder.
Otherwise, you can fix this error and "unlock" PUN's editor scripts by making a change in the project directory (e.g. Create > Folder). PUN will re-check the presence of the asset and create a new one if needed.

### Other versions

To get other versions of PUN, add `#v1` or `#v2` at the end of the url:
- PUN 2 : https://gitlab.com/jonathan.defraiteur/pun-free-unofficial.git#v2
- PUN Classic : https://gitlab.com/jonathan.defraiteur/pun-free-unofficial.git#v1
